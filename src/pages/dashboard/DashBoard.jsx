import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Divider, Grid, Header, Icon, Segment, Statistic } from 'semantic-ui-react'
import DashBoardService from '../../app/api/DashBoardService'
import Moment from 'react-moment'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import ClientesCantidadPorSala from '../../component/dashboard/ClientesCantidadPorSala'
import { connect } from 'react-redux'

const actions={
    openModal,
    closeModal
}
const Dashboard = ({openModal,closeModal}) => {
    const [countCli,setCountCli]=useState([])
    const [fechaHoy,setFechaHoy]=useState(null)
    const fetchClientes = useCallback(async () => {
        const clientes=await DashBoardService.fetchClientesPorSala()
        setCountCli(clientes)
        setFechaHoy(new  Date())
    },[])
    useEffect(()=>{
        fetchClientes()
    },[fetchClientes])
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Reportes</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>DashBoard</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='dashboard'/>Resúmen
                </Header>
            </Divider>
           <Container textAlign='center'>
                <Header as='h2' textAlign='left'>Conteo de clientes al <Moment format='DD / MMM'>{fechaHoy}</Moment></Header>
               <Divider/>
               <Grid stackable>
                      <Grid.Row>
                      {countCli.map((item,i)=>(
                          <Grid.Column key={i} width={4}>
                              <Segment color={item.color}>
                              <Statistic color={item.color}>
                                <Statistic.Value>
                                <Icon name='users' size='big'/><br/>
                                    {item.cantidad}
                                </Statistic.Value>
                                <Statistic.Label><Button color={item.color} size='mini' content={item.nombreSala} onClick={()=>openModal(<ClientesCantidadPorSala id={item.id}/>)}/></Statistic.Label>
                                </Statistic>
                              </Segment><Divider/>
                          </Grid.Column>
                           ))}
                      </Grid.Row> 
               </Grid>
           </Container>
        </Segment>
    )
}

export default connect (null,actions) (Dashboard)
