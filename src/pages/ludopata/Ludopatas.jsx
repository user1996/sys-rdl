import React from 'react'
import { Breadcrumb, Container, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import LudopataForm from '../../component/ludopata/LudopataForm'

const Ludopatas = () => {
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Busqueda</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Ludopatas</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>
                    Busqueda de cliente
                </Header>
            </Divider>
           <Container textAlign='center'>
            <LudopataForm/>
           </Container>
        </Segment>
    )
}

export default Ludopatas
