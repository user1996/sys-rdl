import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Dimmer, Divider, Header, Icon, Popup, Segment } from 'semantic-ui-react'
import EmpresaService from '../../app/api/EmpresaService'
import TableComponent from '../../component/table/TableComponent'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import { connect } from 'react-redux'
import EmpresaForm from '../../component/empresa/EmpresaForm'
import LoadingComponent from '../../component/common/LoadingComponent'
import {ShowMessageAlert} from '../../utils/alert'
const actions={
    openModal,
    closeModal
}
const Empresa = ({openModal,closeModal}) => {
    const handleCreateOrEdit=async (values)=>{
        const empresasUpdateList=[...empresas]  
        try {
            if(values.idEmpresa){
                const updateEmpresa=await EmpresaService.updateEmpresa(values)
                const index=empresasUpdateList.findIndex((a)=>a.idEmpresa===values.idEmpresa)
                empresasUpdateList[index]=updateEmpresa
                ShowMessageAlert('Empresa Actualizada',true,"Aviso")
            }else{
                const newEmpresa=await EmpresaService.saveEmpresa(values)
                empresasUpdateList.push(newEmpresa)
                ShowMessageAlert('Empresa Registrada',true,"Aviso")
            }
            setEmpresas(empresasUpdateList)
            closeModal()
        } catch (error) {
            ShowMessageAlert('Error  '+error,false,"Error ")
        }
    }
    const columns=[
        {
            Header: 'Empresa',
            accessor: 'nombreEmpresa',
        },
        {
            Header:'Estado',
            Cell:row=>{
                let estado=row.row.original.estado
            return (<Popup content={estado?'Activo':'Inactivo'} trigger={<Icon color={estado?'green':'red'} name={estado?'check':'warning'}/>}/>)
            }
        },
        {
            Header:'Acciones',
            accessor:'idEmpresa',
            Cell:({cell})=>(
            <Button color='orange' size='small' icon='edit' onClick={()=>openModal(<EmpresaForm id={cell.row.values.idEmpresa} handleSubmit={handleCreateOrEdit} />)}/>
            )
        }
    ]
    const [loading,setLoading]=useState(true)
    const [empresas,setEmpresas]=useState([])
    const fetchEmpresas=useCallback(async()=>{
        try {
            const empresas=await EmpresaService.fetchEmpresas()
            if(empresas) setEmpresas(empresas)
        } catch (error) {
            ShowMessageAlert("Error : "+error,false,"Error")
        }
        setLoading(false)
        },[])
    useEffect(()=>{
        fetchEmpresas()
    },[fetchEmpresas])
    
    return (
        <Segment>
            <Dimmer active={loading}>
                <LoadingComponent content="Loading Empresas..." />
            </Dimmer>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Mantenimiento</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Empresas</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>Lista de Empresas
                </Header>
            </Divider>
            <Segment textAlign='center'>
               <Button size='large' content="Nueva Empresa" icon='warehouse' color='grey' onClick={()=>openModal(<EmpresaForm handleSubmit={handleCreateOrEdit}/>)}/>
           </Segment>
           <Container textAlign='center'>
               <TableComponent columns={columns} data={empresas}/>
           </Container>
        </Segment>
    )
}

export default connect (null,actions) (Empresa)
