import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Container, Divider, Segment } from 'semantic-ui-react'
import IngresoSalidaService from '../../app/api/IngresoSalidaService'
import { ShowMessageAlert } from '../../utils/alert'
import TableComponent from '../../component/table/TableComponent'
const ReporteAsistencia = () => {
    const columns=[
        {
            Header: 'Documento',
            accessor: 'documento',
        },
        {
            Header:'Nombres y Apellidos',
            accessor:'datosPersona'
        },
        {
            Header:'Fecha',
            accessor:'fecha'
        },
        {
            Header:'Hora',
            accessor:'hora'
        },
        {
            Header:'Sala',
            accessor:'sala'
        }
    ]
    const [asistencia,setAsistencia]=useState([])
    const fetchaIngresoSalida=useCallback(async()=>{
        try {
            const ingreso=await IngresoSalidaService.fetchAll()
            setAsistencia(ingreso)
        } catch (error) {
            ShowMessageAlert("Error : "+error,false,"Error")
        }
        },[])
        useEffect(()=>{
            fetchaIngresoSalida()
        },[fetchaIngresoSalida])

    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Asistencia</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Reporte de Asistencia</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
            </Divider>
           <Container textAlign='center'>
               <TableComponent data={asistencia} columns={columns} />
           </Container>
        </Segment>
    )
}

export default ReporteAsistencia
