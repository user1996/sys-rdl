import React from 'react'
import { Breadcrumb, Container, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import IngresoSalidaForm from '../../component/asistencia/IngresoSalidaForm'

const IngresoSalida = () => {
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Asistencia</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Ingreso y Salida</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>
                    Dni del Trabajador
                </Header>
            </Divider>
           <Container textAlign='center'>
            <IngresoSalidaForm/>
           </Container>
        </Segment>
    )
}

export default IngresoSalida
