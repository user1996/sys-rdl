import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Dimmer, Divider, Header, Icon, List, Segment} from 'semantic-ui-react'
import { connect } from 'react-redux';
import ClienteNoGrato from '../../app/api/ClienteNoGrato'
import {TableComponent} from '../../component/table/TableComponent'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import NoGratoForm from '../../component/cliente/NoGratoForm'
import Moment from 'react-moment'
import LoadingComponent from '../../component/common/LoadingComponent'
import {ShowMessageAlert} from '../../utils/alert'

const actions={
    openModal,
    closeModal
}

const NoGrato = ({openModal,closeModal}) => {
    const [loading,setLoading]=useState(true)
    const handleDeleteClient=async (id)=>{
        try {
            let clienteUpdateList=[...clientes]
            await ClienteNoGrato.deleteClienteNoGrato(id)
            clienteUpdateList=clienteUpdateList.filter((a)=>a.id!==id)
            setClientes(clienteUpdateList)
            ShowMessageAlert("Cliente Eliminado !!!",true,"Aviso")
        } catch (error) {
            ShowMessageAlert("Error : "+error,false,"Error")
        }
    }
    const handleCreateOrEdit=async (values)=>{
        const clienteUpdateList=[...clientes]
        try {
            if(values.id){
                const updateCliente=await ClienteNoGrato.updateClienteNoGrato(values)
                const index=clienteUpdateList.findIndex((a)=>a.id===values.id)
                clienteUpdateList[index]=updateCliente
                ShowMessageAlert("Cliente Actualizado !!!",true,"Aviso")
            }else{
                const newCliente=await ClienteNoGrato.addClienteNoGrato(values)
                clienteUpdateList.push(newCliente)
                ShowMessageAlert("Cliente Registrado",true,"Aviso")
            }
            setClientes(clienteUpdateList)
        } catch (error) {
            ShowMessageAlert("Error : "+error,error,"Error ")
        }
        closeModal()
    }
    const columns=[
        {
            Header: 'Documento',
            accessor: 'documento'
        },
        {
            Header: 'Fecha registro',
            accessor: 'registro',
            Cell:row=><Moment format="YYYY/MM/DD hh:mm:ss A">{row.row.original.registro}</Moment>
        },
        {
            Header: 'Observaciones',
            accessor: 'obseraciones',
            Cell: row =>
                row.row.original.observaciones.map((route,i) => (
                   <List key={i+1} divided relaxed>
                       <List.Item>
                            <List.Icon name='caret right' size='small' verticalAlign='middle' />
                            <List.Content>{route}</List.Content>
                       </List.Item>
                   </List>
                ))
        },
        {
            Header:'Acciones',
            accessor:'id',
            Cell:({cell})=>(<>
            <Button color='orange' size='small' icon='edit' onClick={()=>openModal(<NoGratoForm id={cell.row.values.id} handleSubmit={handleCreateOrEdit}/>)}/>
            <Button color='red' icon='trash' size='small' onClick={()=>handleDeleteClient(cell.row.values.id)}/></>
            )
        }
    ]
    const [clientes,setClientes]=useState([])
    const fetchClientes=useCallback(async ()=>{
        try {
            const clientes=await ClienteNoGrato.fetchClientesNoGratos()
            if(clientes) setClientes(clientes)
        } catch (error) {
            ShowMessageAlert("Error  : "+error,true,"Error")
        }
        setLoading(false)
    },[])
    useEffect(()=>{
        fetchClientes()
    },[fetchClientes])
    let clientesList=<h4>No hay clientes registrados</h4>
    if(clientes && clientes.length>0){
        clientesList=(
            <TableComponent columns={columns} data={clientes}/>
        )
    }
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Registro</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Cliente no grato</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='warning sign'/>
                    Lista de clientes no permitidos
                </Header>
            </Divider>
            <Segment textAlign='center'>
            <Button size='large' content="Nuevo Cliente" icon='add user' color='grey' onClick={()=>openModal(<NoGratoForm handleSubmit={handleCreateOrEdit}/>)}/>
            </Segment>
            <Container textAlign='center'>
            {clientesList}
            <Dimmer active={loading}>
                <LoadingComponent content="Loading Clientes..." />
            </Dimmer>
           </Container>
        </Segment>
    )
}

export default connect (null,actions) (NoGrato)
