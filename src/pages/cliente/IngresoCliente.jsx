import React, { useCallback, useEffect, useState } from 'react';
import { Breadcrumb, Container, Dimmer, Divider, Header, Icon, Popup, Segment, Form, Button } from 'semantic-ui-react';
import TableComponent from '../../component/table/TableComponent'
import Moment from 'react-moment';
import ClienteService from '../../app/api/ClienteService'
import { ShowMessageAlert } from '../../utils/alert';
import LoadingComponent from '../../component/common/LoadingComponent';
import { Form as FinalForm, Field } from 'react-final-form';
import { combineValidators, composeValidators, isRequired } from 'revalidate';
import TextInput from '../../component/form/TextInput';
import ExcelGenerator from '../../component/excel/ExcelGenerator'

const columns = [
    {
        Header: 'Documento',
        accessor: 'dni'
    },
    {
        Header: 'Datos',
        accessor: 'datos'
    },
    {
        Header: 'Fecha Ingreso',
        accessor: 'ingresoEl',
        Cell: row => <Moment format="YYYY/MM/DD hh:mm:ss A">{row.row.original.ingresoEl}</Moment>
    },
    {
        Header: 'Sala',
        accessor: 'sala'
    },
    {
        Header: 'Estado',
        accessor: 'esLudopata',
        Cell: row => {
            let estado = row.row.original.esLudopata
            return (<Popup content={estado ? 'Ludopata' : 'Permitido'} trigger={<Icon color={estado ? 'red' : 'green'} name={estado ? 'warning' : 'check'} />} />)
        }
    },
]
const IngresoCliente = () => {

    const validate = combineValidators({
        fechaDesde: composeValidators(
            isRequired({ message: 'Seleccione fecha' }),
        )(),
        fechaHasta: composeValidators(
            isRequired({ message: 'Seleccione fecha' }),
        )()
    })

    const [loading, setLoading] = useState(true)
    const [ingresoCliente, setIngresoCliente] = useState([])
    const fetchIngresoCliente = useCallback(async () => {
        try {
            const ingresos = await ClienteService.fetchClienteStream()
            if (ingresos) setIngresoCliente(ingresos)
        } catch (error) {
            ShowMessageAlert("Error : " + error, false, "Error")
        }
        setLoading(false)
    }, [])
    useEffect(() => {
        fetchIngresoCliente()
    }, [fetchIngresoCliente])

    const handleBusquedaCliente = async (value) => {
        const queryParam = {
            fechaDesde : value.fechaDesde,
            fechaHasta : value.fechaHasta
        }
        await ClienteService.fetchClienteStreamDate(queryParam).then((data)=>{
            setIngresoCliente(data)
            if (data){
                ShowMessageAlert(data.length + " registros encontrados", true, "Exito")
            }
        })
    }

    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Clientes e ingreso</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron' />
                <Breadcrumb.Section active>Ingreso de clientes</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='warning sign' />
                    Lista de clientes
                </Header>
            </Divider>
            <Container textAlign="center">
                <Segment textAlign='center'>
                    <FinalForm
                        initialValues={{ 'fecha': '' }}
                        onSubmit={(value) => handleBusquedaCliente(value)}
                        validate={validate}
                        render={({ handleSubmit, form, submitting, invalid, pristine, dirtySinceLastSubmit }) => (
                            <Form id="frmludo" onSubmit={event => {
                                handleSubmit(event).then(() => {
                                    form.reset();
                                })
                            }} error>
                                <Header as="h5" content='Busqueda por fecha' color="grey" textAlign="center" /><br />
                                <Field name="fechaDesde" component={TextInput} type="date" label ={'Desde'}/><br />
                                <Field name="fechaHasta" component={TextInput} type="date" label ={'Hasta'}/><br />
                                <Button
                                    fluid
                                    disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                                    loading={submitting}
                                    color='grey'
                                    content='Buscar'
                                />
                            </Form>
                        )}
                    />
                </Segment>
            </Container>
            <Container textAlign='center'>
                <Dimmer active={loading}>
                    <LoadingComponent content="Cargando Clientes..." />
                </Dimmer>
                <Divider/>
                <ExcelGenerator data={ingresoCliente} sheetname={'Reporte de Ingreso y salida de clientes'}/>
                <TableComponent columns={columns} data={ingresoCliente} />
            </Container>
        </Segment>
    )
}
export default IngresoCliente