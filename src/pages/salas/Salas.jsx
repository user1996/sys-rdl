import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Dimmer, Divider, Header, Icon, Popup, Segment } from 'semantic-ui-react'
import SalaService from '../../app/api/SalaService'
import TableComponent from '../../component/table/TableComponent'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import { connect } from 'react-redux'
import SalaForm from '../../component/sala/SalaForm'
import LoadingComponent from '../../component/common/LoadingComponent'
import {ShowMessageAlert} from '../../utils/alert'

const actions={
    openModal,
    closeModal
}
const Salas = ({openModal,closeModal}) => {
    const columns=[
        {
            Header: 'Sala',
            accessor: 'nombreSala'
        },
        {
            Header: 'Empresa',
            accessor: 'nombreEmpresa'
        },
        {
            Header:'Estado',
            Cell:row=>{
                let estado=row.row.original.estado
            return (<Popup content={estado?'Activo':'Inactivo'} trigger={<Icon color={estado?'green':'red'} name={estado?'check':'warning'}/>}/>)
            }
        },
        {
            Header:'Acciones',
            accessor:'idSala',
            Cell:({cell})=>(<>
            <Button color='orange' size='small' icon='edit' onClick={()=>openModal(<SalaForm id={cell.row.values.idSala} handleSubmit={handleCreateOrEdit}/>)}/></>
            )
        }
    ]
    const handleCreateOrEdit=async (values)=>{
        const salasUpdateList=[...salas]  
        try {
            if(values.idSala){
                const updateSala=await SalaService.updateSala(values)
                const index=salasUpdateList.findIndex((a)=>a.idSala===values.idSala)
                salasUpdateList[index]=updateSala
                ShowMessageAlert('Sala Actualizada',true,"Aviso")
            }else{
                const newSala=await SalaService.saveSala(values)
                salasUpdateList.push(newSala)
                ShowMessageAlert('Sala Registrada',true,"Aviso")
            }
            setsalas(salasUpdateList)
            closeModal()
        } catch (error) {
            ShowMessageAlert('Error  '+error,false,"Error !!!")
        }
    }
    const[loading,setLoading]=useState(true)
    const [salas,setsalas]=useState([])
    const fetchSalas=useCallback(async ()=>{
        try {
            const salas=await SalaService.fetchSalas()
            if(salas) setsalas(salas)
        } catch (error) {
            ShowMessageAlert("Error : "+error,false,"Error !!!")
        }
        setLoading(false)
    },[])
    useEffect(()=>{
        fetchSalas()
    },[fetchSalas])
    return (
        <Segment>
            <Dimmer active={loading}>
                <LoadingComponent content="Loading Salas..." />
            </Dimmer>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Mantenimiento</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Salas</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>Lista de Salas
                </Header>
            </Divider>
            <Segment textAlign='center'>
               <Button size='large' content="Nueva sala" icon='plus' color='grey' onClick={()=>openModal(<SalaForm handleSubmit={handleCreateOrEdit}/>)}/>
           </Segment>
           <Container textAlign='center'>
               <TableComponent columns={columns} data={salas}/>
           </Container>
        </Segment>
    )
}

export default connect (null,actions) (Salas)
