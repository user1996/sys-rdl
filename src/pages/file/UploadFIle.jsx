import React, { useState } from 'react'
import { Breadcrumb, Button, Dimmer, Divider, Header, Icon, Segment } from 'semantic-ui-react'
import ClienteService from '../../app/api/ClienteService'
import LoadingComponent from '../../component/common/LoadingComponent'

const UploadFile = () => {
    const [loading,setLoading]=useState(false)
    const fileInputRef = React.createRef();
    const [pdfFile,setPdfFile]=useState(null)
    const [label,setLabel]=useState('No hay ningun archivo cargado')
    const [estado,setEstado]=useState(false)
    const fileChange = (e) => {
        const files=e.target.files
        Array.from(files).forEach(file=>{
            setPdfFile(file)
            setLabel(file.name)
            setEstado(true)
        })
      }
      const handleUpload=async()=>{
          setLoading(true)
          const upload=await ClienteService.uploadPDf(pdfFile).then()
          setEstado(upload)
          setLabel(upload?'Archivo cargado con exito !!':'Archivo no es PDF')
          setEstado(false)
          setLoading(false)
      }
    return (
        <Segment>
            <Dimmer active={loading}>
                <LoadingComponent content="Subiendo Archivo..." />
            </Dimmer>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Mantenimiento</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Subir PDF</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='file'/>Archivos en el Servidor
                </Header>
            </Divider>
            <Segment placeholder textAlign='center'>
               <Header icon>
                    <Icon color={estado?'red':'grey'} name='file pdf outline'/>
                    {label}
               </Header>
               <Button disabled={estado} icon='upload' content='Cargar Archivo' labelPosition='left' onClick={()=>fileInputRef.current.click()}/>
                <input hidden ref={fileInputRef} type='file' onChange={fileChange}/><br/>
                <Button disabled={!estado} icon='cloud upload' color='green' onClick={()=>handleUpload()} content='Subir al servidor' />
           </Segment>
        </Segment>
    )
}

export default UploadFile
