import React, { useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Divider, Header, Icon, Popup, Segment } from 'semantic-ui-react'
import useFetchUsuarios from '../../app/hook/useFetchUsuarios'
import UsuarioService from '../../app/api/UsuarioService'
import TableComponent from '../../component/table/TableComponent'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import UsuarioForm from '../../component/usuario/UsuarioForm'
import { connect } from 'react-redux'
import {ShowMessageAlert} from '../../utils/alert'
const actions={
    openModal,
    closeModal
}
const Usuarios = ({openModal,closeModal}) => {
    const columns=[
        {
            Header: 'Documento',
            accessor: 'documento'
        },
        {
            Header: 'Usuario',
            accessor: 'usuario'
        },
        {
            Header: 'Clave',
            accessor: 'clave'
        },
        {
            Header: 'Genero',
            accessor: 'sexo'
        },
        {
            Header: 'Sala',
            accessor: 'sala.nombreSala'
        },
        {
            Header:'Estado',
            Cell:row=>{
                let estado=row.row.original.estado
            return (<Popup content={estado?'Activo':'Inactivo'} trigger={<Icon color={estado?'green':'red'} name={estado?'check':'warning'}/>}/>)
            }
        },
        {
            Header:'Acciones',
            accessor:'id',
            Cell:({cell})=>(<>
            <Button color='orange' size='small' icon='edit' onClick={()=>openModal(<UsuarioForm id={cell.row.values.id} handleSubmit={handleCreateOrEdit}/>)}/></>
            )
        }
    ]
    const [usuarios]=useFetchUsuarios()
    const [usuariosList,setUsuariosList]=useState([])
    useEffect(()=>{
        setUsuariosList(usuarios)
    },[usuarios])
    const handleCreateOrEdit=async (values)=>{
        const usuariosUpdateList=[...usuarios]  
        try {
            if(values.id){
                const updateUsuario=await UsuarioService.updateUsuario(values)
                const index=usuariosUpdateList.findIndex((a)=>a.id===values.id)
                usuariosUpdateList[index]=updateUsuario
                ShowMessageAlert('Usuario Actualizado',true,"Aviso")
            }else{
                const newUsuario=await UsuarioService.saveUsuario(values)
                usuariosUpdateList.push(newUsuario)
                ShowMessageAlert('Usuario Registrado',true,"Aviso")
            }
            setUsuariosList(usuariosUpdateList)
            closeModal()
        } catch (error) {
            ShowMessageAlert('Error : No se puede Registrar',false,"Error")
        }
    }
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Mantenimiento</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Usuarios</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>Lista de Usuarios
                </Header>
            </Divider>
            <Segment textAlign='center'>
               <Button size='large' content="Nuevo Usuario" icon='plus' color='grey' onClick={()=>openModal(<UsuarioForm handleSubmit={handleCreateOrEdit}/>)}/>
           </Segment>
           <Container textAlign='center'>
               <TableComponent columns={columns} data={usuariosList}/>
           </Container>
        </Segment>
    )
}

export default connect (null,actions) (Usuarios)
