import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Button, Container, Header, Icon, Segment } from 'semantic-ui-react'
import { getJwt } from '../../app/config/auth/credentials'
import { connect } from 'react-redux'

const mapState = (state) => ({
    currentUser: state.auth.currentUser,
    token: getJwt(),
  })
const HomePage = ({ currentUser, token, openModal }) => {
    return (
        <Segment inverted textAlign='center' vertical className="masthead">
            <Container text>
                <Header as="h1" inverted>
                    <Icon name='search'/> RDL System
                </Header>
                <Header as="h2" inverted content="Bienvenido al Sistema RDL"/>
                {currentUser && token?(
                    <>
                    <Button size='huge' as={Link} to='/ludopata' inverted content="Ingresar"/>
                    </>
                ):(
                    <>
                    <Button size='huge' as={Link} to='/login' inverted content="Loguearse"/>
                    </>
                )}
            </Container>
        </Segment>
    )
}
HomePage.propTypes = {
    currentUser: PropTypes.object,
    token: PropTypes.string,
  }
  
  HomePage.defaultProps = {
    currentUser: null,
    token: null,
  }
  export default connect(mapState, null)(HomePage)