import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css'
import './index.css';
import {createBrowserHistory} from 'history'
import App from './app/layout/App';
import * as serviceWorker from './serviceWorker';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './app/store/configureStore'
const history = createBrowserHistory();
const store=configureStore()
const rootEl = document.getElementById('root');
const app=(
  <Provider store={store}>
    <Router history={history}>
    <App/>
  </Router>
  </Provider>
)
ReactDOM.render(
  app,
  rootEl
)
// If you want your app to work offline and load faster, you can change xd
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
export default history