import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)
export const ShowMessageAlert =(mensaje,estado,titulo)=>{
    MySwal.fire({
        position:'center',
        icon: !estado?'error':'success',
        title: titulo,
        html:'<h1>'+mensaje+'</h1>',
        showConfirmButton: false,
        timer:3000
      })  
} 