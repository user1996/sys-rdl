import roles from './roles'
import DashBoard from '../pages/dashboard/DashBoard'
import Empresa from '../pages/empresas/Empresa'
import Salas from '../pages/salas/Salas'
import Usuarios from '../pages/usuarios/Usuarios'
import UploadFile from '../pages/file/UploadFIle'
import IngresoCliente from '../pages/cliente/IngresoCliente'
import NoGrato from '../pages/cliente/NoGrato'
import Ludopatas from '../pages/ludopata/Ludopatas'
import IngresoSalida from '../pages/asistencia/IngresoSalida'
import ReporteAsistencia from '../pages/asistencia/ReporteAsistencia'
export default [
    {
        id:1,
        component: DashBoard,
        path:'/dashboard',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles.SUPERVISOR
        ]
    },
    {
        id:2,
        component: Empresa,
        path:'/empresa',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:3,
        component: Salas,
        path:'/sala',
        permission:[
            roles.ADMIN
        ]
    },
    {
        id:4,
        component: Usuarios,
        path:'/usuario',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:5,
        component: UploadFile,
        path:'/upload',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:6,
        component: NoGrato,
        path:'/noGrato',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles.SUPERVISOR
        ]
    },
    {
        id:7,
        component: IngresoCliente,
        path:'/ingresoCliente',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles.SUPERVISOR
        ]
    },
    {
        id:8,
        component: Ludopatas,
        path:'/ludopata',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles["CAJERO(A)"],
            roles["SEG.PUERTA"],
            roles.SUPERVISOR
        ]
    },
    {
        id:9,
        component: IngresoSalida,
        path: '/IngresoSalida',
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles["CAJERO(A)"],
            roles["SEG.PUERTA"],
            roles.SUPERVISOR
        ]
    },
    {
        id:10,
        component: ReporteAsistencia,
        path: '/reporteAsistencia',
        permission:[
            roles.ADMIN,
            roles.SUPERVISOR
        ]
    }
]