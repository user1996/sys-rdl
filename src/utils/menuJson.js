import roles from './roles'

export default [
    {
        id:1,
        text:'Reportes',
        subItems:[
            {
                id:1,
                text:'DashBoard',
                to:'/dashboard',
                icon:'dashboard',
                subItems:[],
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"],
                    roles.SUPERVISOR
                ]
            }
        ],
        permission:[
            roles.ADMIN,
            roles.SUPERVISOR,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:2,
        text:'Matenimiento',
        subItems:[
            {
                id:1,
                text:'Empresas',
                to:'/empresa',
                icon:'warehouse',
                permission:[
                    roles.ADMIN
                ]
            },
            {
                id:2,
                text:'Salas',
                to:'/sala',
                icon:'home',
                permission:[
                    roles.ADMIN,
                ]
            },
            {
                id:3,
                text:'Usuarios',
                to:'/usuario',
                icon:'user plus',
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"]
                ]
            },
            {
                id:4,
                text:'Subir Pdf',
                to:'/upload',
                icon:'file pdf',
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"]
                ]
            }
        ],
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:3,
        text:'Clientes e Ingreso',
        subItems:[
            {
                id:1,
                text:'Cliente No grato',
                to:'/noGrato',
                icon:'warning circle',
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"]
                ]
            },
            {
                id:2,
                text:'Ingreso de clientes',
                to:'/ingresoCliente',
                icon:'sign-in',
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"]
                ]
            }
        ],
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"]
        ]
    },
    {
        id:4,
        text:'Busqueda',
        subItems:[
            {
                id:1,
                text:'Ludopatas',
                to:'/ludopata',
                icon:'user secret',
                permission:[
                    roles.ADMIN,
                    roles["JEFE.SEG"],
                    roles["CAJERO(A)"],
                    roles["SEG.PUERTA"]
                ]
            }
        ],
        permission:[
            roles.ADMIN,
            roles["JEFE.SEG"],
            roles["CAJERO(A)"],
            roles["SEG.PUERTA"]
        ]
    },
    {
        id:5,
        text: 'Asistencia',
        subItems:[
            {
                id:1,
                text:'Ingreso y Salida',
                to:'/IngresoSalida',
                icon:'sync alternate',
                permission:[
                    roles.ADMIN,
                    roles["SEG.PUERTA"]
                ]
            },
            {
                id:2,
                text:'Reporte de Asistencia',
                to:'/reporteAsistencia',
                icon:'file excel',
                permission:[
                    roles.ADMIN,
                    roles["SUPERVISOR"]
                ]
            }
        ],
        permission:[
            roles.ADMIN,
            roles.SUPERVISOR,
            roles["SEG.PUERTA"]
        ]
    }
]