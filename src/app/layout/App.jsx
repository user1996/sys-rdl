import React, { useEffect, useState } from 'react'
import './App.css'
import Routes from '../config/Routes'
import PropTypes from 'prop-types'
import LoadingComponent from '../../component/common/LoadingComponent'
import ModalContainer from '../../component/modal/ModalContainer'
import {getJwt} from '../config/auth/credentials'
import {getUser} from '../store/actions/authActions'
import { connect } from 'react-redux'

const actions={
  getUser
}
const mapState = (state) => ({
  authenticated: state.auth.authenticated,
  currentUser:state.auth.currentUser
})
const App=({getUser,authenticated,currentUser})=> {
  const [appLoaded,setAppLoaded]=useState(false)
  useEffect(()=>{
    const token=getJwt()
    if(token){
      getUser()
    }
    setAppLoaded(true)
  },[getUser])
  if(!appLoaded) return <LoadingComponent content="Cargando..."/>
  return (
    <>
    <ModalContainer/>
    <Routes authenticated={authenticated} currentUser={currentUser}/>
    </>
  )
}
App.propTypes = {
  getUser: PropTypes.func.isRequired,
  authenticated: PropTypes.bool,
}

App.defaultProps = {
  authenticated: false,
}
export default connect(mapState,actions) (App)
