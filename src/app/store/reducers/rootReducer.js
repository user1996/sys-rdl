import {combineReducers} from 'redux'
import modalReducer from './modalReducer'
import authReducer from './authReducer'
const rootReducer=combineReducers({
    modal:modalReducer,
    auth: authReducer,
})
export default rootReducer