import { useEffect, useState } from 'react'
import SalaService from '../api/SalaService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchEmpresa = (id) => {
    const [sala,setSala]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            SalaService.fetchSala(id)
            .then((response)=>{
                setSala(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error :'+error,false,"Error en el Hook")
            })
            setLoading(false)
    },[id])
    return [sala,loading]
}

export default useFetchEmpresa