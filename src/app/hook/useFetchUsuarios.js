import { useEffect, useState } from 'react'
import UsuarioService from '../api/UsuarioService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchUsuarios = () => {
    const [usuarios,setUsuarios]=useState([])
    useEffect(()=>{
            UsuarioService.fetchUsuarios()
            .then((response)=>{
                setUsuarios(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
    },[])
    return [usuarios]
}

export default useFetchUsuarios