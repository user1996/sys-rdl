import { useEffect, useState } from 'react'
import EmpresaService from '../api/EmpresaService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchEmpresas = () => {
    const [empresas,setEmpresas]=useState([])
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
            EmpresaService.fetchEmpresas()
            .then((response)=>{
                setEmpresas(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
            setLoading(false)
    },[])
    return [empresas,loading]
}

export default useFetchEmpresas