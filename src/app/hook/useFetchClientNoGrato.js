import { useEffect, useState } from 'react'
import ClienteNoGrato from '../api/ClienteNoGrato'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchClientNoGrato = (id) => {
    const [cliente,setCliente]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            ClienteNoGrato.fetchClienteNoGrato(id)
            .then((response)=>{
                setCliente(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
            setLoading(false)
    },[id])
    return [cliente,loading]
}

export default useFetchClientNoGrato