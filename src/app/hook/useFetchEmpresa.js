import { useEffect, useState } from 'react'
import EmpresaService from '../api/EmpresaService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchEmpresa = (id) => {
    const [empresa,setEmpresa]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            EmpresaService.fetchEmpresa(id)
            .then((response)=>{
                setEmpresa(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
            setLoading(false)
    },[id])
    return [empresa,loading]
}

export default useFetchEmpresa