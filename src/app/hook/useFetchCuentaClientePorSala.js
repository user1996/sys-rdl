import { useEffect, useState } from 'react'
import DashBoardService from '../api/DashBoardService'
import {ShowMessageAlert} from '../../utils/alert'

const useFetchCuentaClientePorSala = (id) => {
    const [clientes,setClientes]=useState([])
    useEffect(()=>{
        DashBoardService.fetchCantidadClientesPorSala(id)
            .then((response)=>{
                setClientes(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
    },[id])
    return [clientes]
}

export default useFetchCuentaClientePorSala