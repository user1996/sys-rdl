import { useEffect, useState } from 'react'
import UsuarioService from '../api/UsuarioService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchUsuario = (id) => {
    const [ususario,setUsuario]=useState(null)
    useEffect(()=>{
        if(id)
            UsuarioService.fetchUsuario(id)
            .then((response)=>{
                setUsuario(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
    },[id])
    return [ususario]
}

export default useFetchUsuario