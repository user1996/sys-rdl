import { useEffect, useState } from 'react'
import SalaService from '../api/SalaService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchSalas = () => {
    const [salas,setSalas]=useState([])
    useEffect(()=>{
            SalaService.fetchSalas()
            .then((response)=>{
                setSalas(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error : '+error,false,"Error en el Hook")
            })
    },[])
    return [salas]
}

export default useFetchSalas