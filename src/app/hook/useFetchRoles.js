import { useEffect, useState } from 'react'
import RolService from '../api/RolService'
import {ShowMessageAlert} from '../../utils/alert'
const useFetchRoles = () => {
    const [roles,setRoles]=useState([])
    useEffect(()=>{
            RolService.fetchRoles()
            .then((response)=>{
                setRoles(response)
            })
            .catch((error)=>{
                ShowMessageAlert('Error '+error,false,"Error en el Hook")
            })
    },[])
    return [roles]
}

export default useFetchRoles