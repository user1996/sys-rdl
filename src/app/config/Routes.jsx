import React from 'react'
import { Redirect, Route, Switch, withRouter} from 'react-router-dom'
import { Container } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import Navbar from '../../component/navbar/Navbar'
import HomePage from '../../pages/home/HomePage'
import LoginForm from '../../component/auth/LoginForm'
import routesConfig from '../../utils/routesConfig'
import NotFound from '../../component/common/NotFound'
const Routes = ({authenticated,currentUser}) => {
  const listRoutes=[]
  if(currentUser){
    const rol=currentUser.roles[0]
  routesConfig.forEach(item=>{
    if(item.permission && item.permission.filter(i=>i===rol).length>0){
      listRoutes.push(item)
    }
  })
  }
    return (
        <>
        <Route exact path="/" component={HomePage} />
        <Route path="/login" component={LoginForm} />
        <Route
        path="/(.+)"
        render={(props) => (
          authenticated?(
            <>
            <Navbar/>
            <Container style={{marginTop:'7em'}}>
              <Switch>
                {listRoutes.map(item=>(
                  <Route key={item.id} path={item.path} component={item.component}/>
                ))}
                <Route component={NotFound} />
              </Switch>
              </Container>
          </>
          ):(<>
            <Redirect exact to='/login' from={toString(props.location)} /></>
          )
        )}
      />
        </>
    )
}
Routes.propTypes = {
  authenticated: PropTypes.bool,
  currentUser:PropTypes.object
}

Routes.defaultProps = {
  authenticated: false,
  currentUser:{}
}
export default withRouter (Routes)

