export const TOKEN_KEY='token'

//endpoints
export const CLIENTE_ENDPOINT = '/cliente'
export const CLIENTE_NO_GRATO_ENDPOINT = '/noGrato'
export const SALA_ENDPOINT = '/sala'
export const EMPRESA_ENDPOINT = '/empresa'
export const USUARIO_ENDPOINT = '/usuario'
export const ROL_ENDPOINT = '/rol'
export const DASHBOARD_ENDPOINT = '/dashboard'
export const AUTH_ENDPOINT='/login'
export const INGRESO_SALIDA_ENDPOINT='/ingresoSalida'