import {SALA_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getSala=(id)=>`${SALA_ENDPOINT}/${id}`
class SalaService{
    static fetchSalas=()=>baseApi.get(SALA_ENDPOINT)
    static fetchSala=(id)=>baseApi.get(getSala(id))
    static saveSala=(sala)=>baseApi.post(SALA_ENDPOINT,sala)
    static updateSala=(sala)=>baseApi.put(getSala(sala.idSala),sala)
}
export default SalaService