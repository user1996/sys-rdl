import {INGRESO_SALIDA_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
class IngresoSalidaService{
    static fetchAll=()=>baseApi.get(INGRESO_SALIDA_ENDPOINT+'/all')
    static saveIngresoSalida=(documento)=>baseApi.post(INGRESO_SALIDA_ENDPOINT,documento)
}
export default IngresoSalidaService