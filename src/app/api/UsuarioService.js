import {USUARIO_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getUsuario=(id)=>`${USUARIO_ENDPOINT}/${id}`
class SalaService{
    static fetchUsuarios=()=>baseApi.get(USUARIO_ENDPOINT)
    static fetchUsuario=(id)=>baseApi.get(getUsuario(id))
    static saveUsuario=(usuario)=>baseApi.post(USUARIO_ENDPOINT,usuario)
    static updateUsuario=(usuario)=>baseApi.put(getUsuario(usuario.id),usuario)
}
export default SalaService