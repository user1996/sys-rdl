import {EMPRESA_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getEmpresa=(id)=>`${EMPRESA_ENDPOINT}/${id}`
class EmpresaService{
    static fetchEmpresas=()=>baseApi.get(EMPRESA_ENDPOINT)
    static fetchEmpresa=(id)=>baseApi.get(getEmpresa(id))
    static saveEmpresa=(empresa)=>baseApi.post(EMPRESA_ENDPOINT,empresa)
    static updateEmpresa=(empresa)=>baseApi.put(getEmpresa(empresa.idEmpresa),empresa)
}
export default EmpresaService