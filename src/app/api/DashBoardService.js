import {DASHBOARD_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getDash=(id)=>`${DASHBOARD_ENDPOINT}/${id}`
class DashBoardService{
    static fetchClientesPorSala=()=>baseApi.get(DASHBOARD_ENDPOINT)
    static fetchCantidadClientesPorSala=(id)=>baseApi.get(getDash(id))
}
export default DashBoardService