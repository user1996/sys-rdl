import axios from 'axios'
import ndjsonStream from 'can-ndjson-stream'
import { getJwt } from '../config/auth/credentials'
import { TOKEN_KEY } from '../core/appConstants'
import history from '../..'
import {ShowMessageAlert} from '../../utils/alert'
const http=axios.create({
    baseURL: process.env.REACT_APP_API_URL,
})
const httpFetch=(url,method,type)=>{
    const token = getJwt()
   return  fetch(process.env.REACT_APP_API_URL+url,{
       method:method,
       headers:{
            Accept: type,
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token,
       }
   })
}
http.interceptors.response.use(undefined, (error) => {
    if (error.message === 'Network Error' && !error.response) {
      ShowMessageAlert('Network error - make sure the API server is running',false,"Warning")
      window.localStorage.removeItem(TOKEN_KEY)
      window.location='/login'
      //history.push('/login')
      ShowMessageAlert('Your session has expired, please login again',false,"Aviso")
    }
  
    const { status, data, config } = error.response
    if (status === 404) {
      history.push('/notFound')
    }
  
    // eslint-disable-next-line no-prototype-builtins
    if ((status === 400 && config.method === 'get') || data.errors.hasOwnProperty('id')) {
      history.push('/notFound')
    }
  
    if (status === 500) {
      ShowMessageAlert('Server error - check the terminal for more info!',false,"Warning")
    }
  
    throw error.response
  })
  
  // To add token
  http.interceptors.request.use(
    (config) => {
      const token = getJwt()
      if (token) config.headers.Authorization = `Bearer ${token}`
      return config
    },
    (error) => Promise.reject(error)
  )
  
const responseBody=(response)=>response.data

const baseApi={
    get: (url)=>http.get(url).then(responseBody),
    getWithQueryParam: (url, config) => http.get(url, { params: config }).then(responseBody),
    post: (url, body) => http.post(url, body).then(responseBody),
    put: (url, body) => http.put(url, body).then(responseBody),
    delete: (url) => http.delete(url).then(responseBody),
    postForm:(url,file)=>{
        const formData=new FormData()
        formData.append('file',file)
        return http.post(url,formData,{
            headers:{'Content-Type':'multipart/form-data'}
        }).then(responseBody)
    },
    stream1:(url)=>{
        return http.get(url).then(responseBody).then(resp=>{
            return ndjsonStream(resp.body);
        })
    },
    stream:(url)=>httpFetch(url,'GET','application/stream+json').then(resp => {
        return ndjsonStream(resp.body);
      })
}
export default baseApi
