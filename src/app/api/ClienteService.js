import {CLIENTE_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'

class ClienteService{
    static fetchClienteLudopata=(documento)=>baseApi.get(CLIENTE_ENDPOINT+`/consultar/${documento}`)
    static fetchClienteIngreso=()=>baseApi.get(CLIENTE_ENDPOINT)
    static uploadPDf = (photo) => baseApi.postForm(`${CLIENTE_ENDPOINT}/upload`, photo)
    static fetchClienteStream=()=>baseApi.get(`${CLIENTE_ENDPOINT}/stream`)
    static fetchClienteStreamDate=(params)=>baseApi.getWithQueryParam(`${CLIENTE_ENDPOINT}/streamByDate`, params)
}
export default ClienteService
