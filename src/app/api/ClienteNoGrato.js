import {CLIENTE_NO_GRATO_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getClienteNoGrato=(id)=>`${CLIENTE_NO_GRATO_ENDPOINT}/${id}`
class ClienteNoGrato{
    static fetchClientesNoGratos=()=>baseApi.get(CLIENTE_NO_GRATO_ENDPOINT)
    static fetchClienteNoGrato=(id)=>baseApi.get(getClienteNoGrato(id))
    static addClienteNoGrato=(cliente)=>baseApi.post(CLIENTE_NO_GRATO_ENDPOINT,cliente)
    static updateClienteNoGrato=(cliente)=>baseApi.put(getClienteNoGrato(cliente.id),cliente)
    static deleteClienteNoGrato=(id)=>baseApi.delete(getClienteNoGrato(id))
}
export default ClienteNoGrato