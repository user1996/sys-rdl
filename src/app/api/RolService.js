import {ROL_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'
const getRol=(id)=>`${ROL_ENDPOINT}/${id}`
class RolService{
    static fetchRoles=()=>baseApi.get(ROL_ENDPOINT)
    static fetchRol=(id)=>baseApi.get(getRol(id))
    static saveRol=(rol)=>baseApi.post(ROL_ENDPOINT,rol)
    static updateRol=(rol)=>baseApi.put(getRol(rol.id),rol)
}
export default RolService