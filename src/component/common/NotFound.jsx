import React from 'react'
import { Link} from 'react-router-dom'
import { Button, Header, Icon, Segment } from 'semantic-ui-react'

const NotFound = () => {
    let prevUrl = document.referrer;
    if(prevUrl.indexOf(window.location.host) !== -1) {
        // Ir a la página anterior
       console.log(window.history.back())
    }
    return (
        <Segment placeholder>
            <Header icon>
                <Icon name='search'/>
                Oops - Pagina no encontrada.
            </Header>
            <Segment.Inline>
                <Button as={Link} to='/' primary>Regresar al Inicio</Button>
            </Segment.Inline>
        </Segment>
    )
}

export default NotFound
