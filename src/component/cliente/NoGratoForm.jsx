import React, { useEffect, useState } from 'react'
import {combineValidators,isRequired,composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import {Button, Container, Divider, Form,Header, Input, Table} from 'semantic-ui-react'
import PropTypes from 'prop-types'

import useFetchClientNoGrato from '../../app/hook/useFetchClientNoGrato'
import TextInput from '../form/TextInput'
import  ErrorMessage  from '../form/ErrorMessage'

const validate = combineValidators({
    documento: composeValidators(
      isRequired({message:'Documento es obligatorio'})
    )(),
  })
const NoGratoForm = ({id,handleSubmit}) => {
    const [actionLabel,setActionLabel]=useState('Registrar cliente')
    const [cliente,loading]=useFetchClientNoGrato(id)
    const [items, setItems] = useState([])
    const [item, setItem] = useState('')
    useEffect(()=>{
        if(id){
            if(cliente) setItems(cliente.observaciones)
            setActionLabel('Editar cliente')
        }else{
            setItems([])
            setActionLabel('Registrar nuevo cliente')
        }
    },[id,cliente])
    const handleAddingItems=()=>{
        const itemsList=[...items]
        itemsList.push(item)
        setItems(itemsList)
        setItem('')
    }
    const handleDeleteItem=(index)=>{
        let updatedItems = [...items]
        updatedItems=updatedItems.filter((a)=>updatedItems.indexOf(a)!==index)
        setItems(updatedItems)
    }
    const setTest=(value)=>{
        if(value.observaciones){
            value.observaciones=[]
            items.map(i=>(
                value.observaciones.push(i)
            ))
        }else{
            value.observaciones=items
        }
        handleSubmit(value)
    }
    let observaciones=<h4>Sin observaciones</h4>
    if(items.length>0){
        observaciones=(
                <Table celled size='small'>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Motivo</Table.HeaderCell>
                            <Table.HeaderCell>Accion</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                {items.map((item,i)=>(
                <Table.Row key={i}>
                    <Table.Cell>{item}</Table.Cell>
                    <Table.Cell>
                        <Button icon='trash' type="button" color='red' size='mini' onClick={()=>handleDeleteItem(i)}/>
                    </Table.Cell>
                </Table.Row>
            ))}
            </Table.Body>
            </Table>
        )
    }
    return (
        <FinalForm
            onSubmit={(value)=>setTest(value)}
            initialValues={id && cliente}
            validate={validate}
            render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
                <Form onSubmit={handleSubmit} error loading={loading}>
                    <Header as="h2" content={actionLabel} color="grey" textAlign="center"/>
                    <Field name="documento" component={TextInput} placeholder="Documento" />
                    <Field name="nombres" component={TextInput} placeholder="Nombres"/>
                    <Field name="apellidoPaterno" component={TextInput} placeholder="Apellido paterno"/>
                    <Field name="apellidoMaterno" component={TextInput} placeholder="Apellido materno"/>
                    <Input type='text' name='observacion' placeholder='Observaciones ' onChange={(e)=>setItem(e.target.value)} />
                    <Button icon='add' color='green' size='mini'
                    onClick={handleAddingItems}
                    disabled={!item}
                    type='button'/>
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text="Invalid username or password" />
                    )}
                    <Divider/>
                    <Container textAlign='center'>
                        {observaciones}
                    </Container>
                    <Divider/>
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit)}
                        loading={submitting}
                        color='grey'
                        content={actionLabel}
                    />
                </Form>
            )}
        />
    )
}
NoGratoForm.propTypes={
    id:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
NoGratoForm.defaultProps={
    id:null
}
export default NoGratoForm
