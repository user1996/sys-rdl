import React from 'react'
import { Field, Form as FinalForm } from 'react-final-form'
import { combineValidators, composeValidators, createValidator, isRequired } from 'revalidate'
import { Button, Form, Header } from 'semantic-ui-react'

//import TextInput from '../../component/form/TextInput'
import ErrorMessage from '../../component/form/ErrorMessage'
import CursoService from '../../app/api/ClienteService'
import { ShowMessageAlert } from '../../utils/alert'
import TextInput from '../form/TextInput'

const isValidDni = (n) => createValidator(
    message => value => {
        if (value.length < 8) {
            return message
        }
    },
    field => `${field} Debe ser mayor de ${n}`
)
const validate = combineValidators({
    dni: composeValidators(
        isRequired({ message: 'Dni es Obligatorio' }),
        isValidDni(8)({
            message: 'Dni debe tener mas de ' + 8 + ' digitos'
        })
    )()
})

const LudopataForm = () => {
	const initialValue = {dni:''}
    const handleBusquedaCliente = async (values) => {
        if (values) {
            try {
                const response = await CursoService.fetchClienteLudopata(values.dni)
                ShowMessageAlert(response.datosPersona, !response.esLudopata, response.mensajeRespuesta)
            } catch (error) {
                ShowMessageAlert("Error", false, error.statusText)
            }
        }
    }
    return (
        <FinalForm
		initialValues={initialValue}
            onSubmit={(value) => handleBusquedaCliente(value)}
            validate={validate}
            render={({ handleSubmit, form, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
                <Form id="frmludo" onSubmit={event => {
                    handleSubmit(event).then(() => {
                        form.reset();
                    })
                }} error>
                    <Header as="h2" content='Busqueda' color="grey" textAlign="center" />
                    <Field name="dni" component={TextInput} placeholder="Ingrese Dni" />
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text="Invalid username or password" />
                    )}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color='grey'
                        content='Buscar'
                    />
                </Form>
            )}
        />
    )
}

export default LudopataForm
