import React from 'react'
import { Field, Form as FinalForm } from 'react-final-form'
import { combineValidators, composeValidators, createValidator, isRequired } from 'revalidate'
import { Button, Form, Header } from 'semantic-ui-react'

import TextInput from '../../component/form/TextInput'
import ErrorMessage from '../../component/form/ErrorMessage'
import IngresoSalidaService from '../../app/api/IngresoSalidaService'
import {ShowMessageAlert} from '../../utils/alert'

const isValidDni=(n)=>createValidator(
    message=>value=>{
        if(value.length<8){
            return message
        }
    },
    field=>`${field} Debe ser mayor de ${n}`
)
const validate = combineValidators({
    documento: composeValidators(
      isRequired({message:'Dni es Obligatorio'}),
      isValidDni(8)({
          message:'Dni debe tener mas de '+8+' digitos'
      })
    )()
  })
const IngresoSalidaForm = () => {  
    const handleBusquedaUsuario=async(values)=>{
        if(values){
            try {
                const response=await IngresoSalidaService.saveIngresoSalida(values)
                ShowMessageAlert(response.datosPersona+ ' - '+ response.hora,true,'Asistencia registrada !!')
            } catch (error) {
                ShowMessageAlert(error.status,false,error.statusText)
            }
             
        }
    }
    return (       
        <FinalForm
            onSubmit={(value)=>handleBusquedaUsuario(value)}
            validate={validate}
            render={({handleSubmit,form, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
                <Form onSubmit={event=>{
                    handleSubmit(event).then(()=>{
                        form.reset();
                    })
                }} error>
                    <Header as="h2" content='Registro de Asistencia' color="grey" textAlign="center"/>
                    <Field name="documento" component={TextInput} placeholder="Ingrese Dni" />
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text="Invalid username or password" />
                    )}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color='grey'
                        content='Buscar'
                    />
                </Form>
            )}
        />
    )
}

export default IngresoSalidaForm
