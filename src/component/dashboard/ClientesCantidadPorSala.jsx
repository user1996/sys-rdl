import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb, Container, Divider, Header, Icon, Popup, Segment } from 'semantic-ui-react'
import useFetchCuentaClientePorSala from '../../app/hook/useFetchCuentaClientePorSala'
import TableComponent from '../table/TableComponent'

const ClientesCantidadPorSala = ({id}) => {
   
     const[clientes]=useFetchCuentaClientePorSala(id);
     const columns=[
        {
            Header: 'Dni',
            accessor: 'dni',
        },
        {
            Header: 'Nombres',
            accessor: 'nombres',
        },
        {
            Header: 'Paterno',
            accessor: 'apellidoPaterno',
        },
        {
            Header: 'Materno',
            accessor: 'apellidoMaterno',
        },
        {
            Header: 'Estado',
            accessor:'esLudopata',
            Cell:row=>{
                let estado=row.row.original.esLudopata
            return (<Popup content={estado?'Ludopata':'Permitido'} trigger={<Icon color={estado?'red':'green'} name={estado?'warning':'check'}/>}/>)
            }
        },
    ]
    let content =
        <Header as='h2' textAlign='center'>
            Sin Registros
            <Header.Subheader>
                Esta vacio
            </Header.Subheader>
        </Header>
    if(clientes && clientes.length>0){
        content=(
            <TableComponent columns={columns}data={clientes} />
        )
    }
    return (
        <Segment>
            <Breadcrumb size='small'>
                <Breadcrumb.Section>Detalle</Breadcrumb.Section>
                <Breadcrumb.Divider icon='right chevron'/>
                <Breadcrumb.Section active>Cantidad</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as='h4'>
                    <Icon name='list alternate outline'/>Lista de Clientes
                </Header>
            </Divider>
            <Container textAlign='center'>
                {content}
            </Container>
        </Segment>
    )
}
ClientesCantidadPorSala.propTypes={
    id:PropTypes.string
}
export default ClientesCantidadPorSala
