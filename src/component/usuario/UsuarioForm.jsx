import React, {useEffect, useState } from 'react'
import {combineValidators,isRequired,composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import PropTypes from 'prop-types'
import TextInput from '../form/TextInput'
import SelectInput from '../form/SelectInput'
import CheckBox from '../form/CheckBox'
import RadioButton from '../form/RadioButton'
import useFetchUsuario from '../../app/hook/useFetchUsuario'
import useFetchSalas from '../../app/hook/useFetchSalas'
import useFetchRoles from '../../app/hook/useFetchRoles'
import { Button, Form, Header} from 'semantic-ui-react'
import ErrorMessage from '../form/ErrorMessage'

const validate = combineValidators({
    documento: composeValidators(
        isRequired({message:'Documento de identidad es obligatorio'})
      )(),
    nombresApellidos: composeValidators(
        isRequired({message:'Nombres y apellidos de usuario son obligatorios'})
      )(),
    usuario: composeValidators(
      isRequired({message:'Nombre de usuario es obligatorio'})
    )(),
    key: composeValidators(
        isRequired({message:'clave de usuario es obligatoria'})
      )(),
    "rol.id": composeValidators(
        isRequired({message:'Selecciona rol'})
      )(),
    "sala.idSala": composeValidators(
        isRequired({message:'Selecciona Sala'})
      )(),
  })
const UsuarioForm = ({id,handleSubmit}) => {
    const [rolesList,setRolesList]=useState([])
    const [salasList,setSalasList]=useState([])
    const [actionLabel,setActionLabel]=useState(null)
    const [usuario,loading]=useFetchUsuario(id)
    const [roles]=useFetchRoles()
    const [salas]=useFetchSalas()

    const setTest=(value)=>{
        handleSubmit(value)
    }

    useEffect(()=>{
        if(roles && salas){
            const salasList=[]
            salas.forEach((item)=>{
                const sala={
                    key:item.idSala,
                    text: item.nombreSala,
                    value:item.idSala,
                    disabled:!item.estado,
                    label:{color:item.estado?'green':'red',empty:true,circular:true}
                }
                salasList.push(sala)
            })
            setSalasList(salasList)
            const rolesList=[]
            roles.forEach((item)=>{
                const rol={
                    key:item.id,
                    text: item.nombre,
                    value:item.id,
                    disabled:!item.estado,
                    label:{color:item.estado?'green':'red',empty:true,circular:true}
                }
                rolesList.push(rol)
            })
            setRolesList(rolesList)
        }
        if(id){
            if(usuario){
                setActionLabel('Editar Usuario')
            }   
        }else{
            setActionLabel('Registrar Usuario')
        }
    },[id,usuario,roles,salas])
    return (
        <FinalForm
        onSubmit={(value)=>setTest(value)}
        initialValues={id && usuario}
        validate={validate}
        render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
            <Form onSubmit={handleSubmit} error loading={loading}>
                <Header icon={id?'edit':'plus'} as="h2" content={actionLabel} color="grey" textAlign="center"/>
                <Field name="documento" component={TextInput} placeholder="documento" />
                <Form.Group widths='equal'>
                    <Field name="nombresApellidos" component={TextInput} placeholder="Nombres y Apellidos"/>
                </Form.Group>
                <Field name="usuario" component={TextInput} placeholder="Nombre de usuario" />
                <Field name="key" component={TextInput} type='password' placeholder="Key de usuario" />
                <Field name="sexo" component={RadioButton} label="M" value="M" type='radio'/><Field name="sexo" component={RadioButton} label="F" value="F" type='radio'/>
                <Field name="rol.id" component={SelectInput} placeholder="Selecciona un rol" options={rolesList}/>
                <Field name="sala.idSala" component={SelectInput} placeholder="Selecciona una Sala" options={salasList}/>
                <Field name="estado" type='checkbox' label='Activo' component={CheckBox}/>
                {submitError && !dirtySinceLastSubmit && (
                    <ErrorMessage error={submitError} text="Invalid username or password" />
                )}
                <Button
                    fluid
                    disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                    loading={submitting}
                    color='grey'
                    content={actionLabel}
                />
            </Form>
        )}
    />
    )
}
UsuarioForm.propTypes={
    id:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
UsuarioForm.defaultProps={
    id:null
}
export default UsuarioForm
