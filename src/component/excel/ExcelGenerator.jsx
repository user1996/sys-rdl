import React from 'react'
import ReactExport from 'react-export-excel'

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ExcelGenerator = ({data,sheetname}) => {
    return (
        <ExcelFile>
                <ExcelSheet data={data} name={sheetname}>
                    <ExcelColumn label="Documento" value="dni"/>
                    <ExcelColumn label="Nombres Apellidos" value="datos"/>
                    <ExcelColumn label="Fecha" value="ingresoEl"/>
                    <ExcelColumn label="Ludopata" value="esLudopata"/>
                    <ExcelColumn label="Sala" value="sala"/>
                    <ExcelColumn label="Registrado por" value="registradoPor"/>
                </ExcelSheet>
            </ExcelFile>
    )
}

export default ExcelGenerator
