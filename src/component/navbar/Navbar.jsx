import React, { useEffect, useState } from 'react'
import {Link} from 'react-router-dom'
import { Button, Container, Dropdown, Grid, Icon, Menu,Image } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { logout } from '../../app/store/actions/authActions'
import menuJson from '../../utils/menuJson'

const estado={
    dropdownMenuStyle: {
        topNavbar: {
          display: "none"
        },
        firstNavbar: {
          display: "none"
        },
        secondNavbar: {
          display: "none"
        }
      }
}
    const mapState = (state) => ({
        currentUser: state.auth.currentUser,
    })
  
    const actions = {
        logout,
    }
const Navbar = ({ currentUser, logout }) => {
    const rol=currentUser.roles[0]
    const [state,setState]=useState(estado)
    useEffect(()=>{
        setState(estado)
    },[])
    const handleToggleDropdownMenu = navKey => {
        let newState = Object.assign({}, state)
        if (newState.dropdownMenuStyle[navKey].display === "none") {
        newState.dropdownMenuStyle[navKey] = { display: "flex" }
        } else {
        newState.dropdownMenuStyle[navKey] = { display: "none" }
        }
        setState(newState)
    }
    const newMenu=[]
    const valida =(item,rol)=>{
        let newSubItems=[]
        item.subItems.forEach(sub=>{
            if(sub.permission.filter((per)=>per===rol).length>0){
                newSubItems.push(sub)
            }
            item.subItems=newSubItems
        })
        return item
    }
    menuJson.forEach(item=>{
        if(item.permission && item.permission.filter((item)=>item===rol).length>0){
                newMenu.push(valida(item,rol))
          }
    })
    return (
        <>
            <Grid padded className="tablet computer only">
            <Menu borderless inverted fixed="top">
                <Container>
                    <Menu.Item header as={Link} to='/'>RDL System ©</Menu.Item>
                    {newMenu.map((item)=>(
                        <Dropdown icon={item.icon} item key={item.id} text={item.text}>
                            <Dropdown.Menu>
                            {item.subItems.map((sub)=>(
                                <Dropdown.Item key={sub.id} icon={sub.icon} as={Link} to={sub.to} text={sub.text}/>
                            ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    ))}
                    <Menu.Item as='a' position='right'>
                    <Image avatar size='mini' spaced="right" src={`/assets/img/${currentUser.genero}.png`} />
                    <Dropdown pointing="top left" text={currentUser.sub}>
                        <Dropdown.Menu>
                        <Dropdown.Item icon="address card outline" text={`Acceso : ${rol}`}/>
                        <Dropdown.Item text="Logout" icon="log out" onClick={()=>logout()}/>
                        </Dropdown.Menu>
                    </Dropdown>
                    </Menu.Item>
                </Container>
            </Menu>
        </Grid>
        <Grid padded className="mobile only">
            <Menu borderless inverted fixed='top'>
                <Menu.Item header as={Link} to='/'>RDL System ©</Menu.Item>
                <Menu.Menu position='right'>
                    <Menu.Item>
                        <Button icon basic inverted toggle onClick={()=>handleToggleDropdownMenu("topNavbar")}><Icon name='content'/></Button>
                    </Menu.Item>
                </Menu.Menu>
                <Menu vertical inverted borderless fluid style={state.dropdownMenuStyle.topNavbar}>
                    {newMenu.map((item)=>(
                        <Dropdown key={item.id} item text={item.text}>
                            <Dropdown.Menu>
                                {item.subItems.map(sub=>(
                                    <Dropdown.Item key={sub.id} icon={sub.icon.ic} as={Link} to={sub.to} text={sub.text}/>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    ))}
                    
                    <Dropdown item icon='user' text={currentUser.sub}>
                        <Dropdown.Menu>
                        <Dropdown.Item icon="address card outline" text={`Acceso : ${rol}`}/>
                        <Dropdown.Item text="Logout" icon="log out" onClick={()=>logout()}/>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu>
            </Menu>
        </Grid>
        </>
    )
}
Navbar.propTypes = {
    currentUser: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
  }
  export default connect(mapState, actions)(Navbar)