import React, { useEffect, useState } from 'react'
import {combineValidators,isRequired,composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import PropTypes from 'prop-types'
import TextInput from '../form/TextInput'
import CheckBox from '../form/CheckBox'
import useFetchEmpresa from '../../app/hook/useFetchEmpresa'
import { Button, Form, Header } from 'semantic-ui-react'
import ErrorMessage from '../form/ErrorMessage'
const validate = combineValidators({
    nombreEmpresa: composeValidators(
      isRequired({message:'Nombre de empresa es obligatorio'})
    )(),
  })
const EmpresaForm = ({id,handleSubmit}) => {
    const [actionLabel,setActionLabel]=useState('Agregar Empresa')
    const [empresa,loading]=useFetchEmpresa(id)
    useEffect(()=>{
        if(id){
            setActionLabel('Editar Empresa')
        }
    },[id,empresa])
    return (
        <FinalForm
        onSubmit={(value)=>handleSubmit(value)}
        initialValues={id && empresa}
        validate={validate}
        render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
            <Form onSubmit={handleSubmit} error loading={loading}>
                <Header as="h2" content={actionLabel} color="grey" textAlign="center"/>
                <Field name="nombreEmpresa" component={TextInput} placeholder="Empresa" />
                <Field name="estado" type='checkbox' label='Activo' component={CheckBox}/>
                {submitError && !dirtySinceLastSubmit && (
                    <ErrorMessage error={submitError} text="Invalid username or password" />
                )}
                <Button
                    fluid
                    disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                    loading={submitting}
                    color='grey'
                    content={actionLabel}
                />
            </Form>
        )}
    />
    )
}
EmpresaForm.propTypes={
    id:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
EmpresaForm.defaultProps={
    id:null
}
export default EmpresaForm
