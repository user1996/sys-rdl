import React, {useEffect, useState } from 'react'
import {combineValidators,isRequired,composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import PropTypes from 'prop-types'
import TextInput from '../form/TextInput'
import SelectInput from '../form/SelectInput'
import CheckBox from '../form/CheckBox'
import useFetchSala from '../../app/hook/useFetchSala'
import useFetchEmpresas from '../../app/hook/useFetchEmpresas'
import { Button, Form, Header } from 'semantic-ui-react'
import ErrorMessage from '../form/ErrorMessage'

const validate = combineValidators({
    nombreSala: composeValidators(
      isRequired({message:'Nombre de Sala es obligatorio'})
    )(),
    "empresa.idEmpresa":composeValidators(
        isRequired({message:'Seleccione Empresa'})
      )()
  })
const SalaForm = ({id,handleSubmit}) => {
    const [empresasList,setEmpresasList]=useState([])
    const [actionLabel,setActionLabel]=useState(null)
    const [sala,loading]=useFetchSala(id)
    const [empresas]=useFetchEmpresas()

   
    useEffect(()=>{
        if(empresas){
            const empresaList=[]
            empresas.forEach((item)=>{
                const empresa={
                    key:item.idEmpresa,
                    text: item.nombreEmpresa,
                    value:item.idEmpresa,
                    disabled:!item.estado,
                    label:{color:item.estado?'green':'red',empty:true,circular:true}
                }
                empresaList.push(empresa)
            })
            setEmpresasList(empresaList)
        }
        if(id){setActionLabel('Editar Sala')}else{setActionLabel('Registrar Sala')}
    },[id,sala,empresas])
    return (
        <FinalForm
        onSubmit={(value)=>handleSubmit(value)}
        initialValues={id && sala}
        validate={validate}
        render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
            <Form onSubmit={handleSubmit} error loading={loading}>
                <Header as="h2" content={actionLabel} color="grey" textAlign="center"/>
                <Field name="nombreSala" component={TextInput} placeholder="Sala" />
                <Field name="color" component={TextInput} placeholder="Color Sala - Opcional" />
                <Field name="empresa.idEmpresa" component={SelectInput} placeholder="Selecciona unaEmpresa" options={empresasList}/>
                <Field name="estado" type='checkbox' label='Activo' component={CheckBox}/>
                {submitError && !dirtySinceLastSubmit && (
                    <ErrorMessage error={submitError} text="Invalid username or password" />
                )}
                <Button
                    fluid
                    disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                    loading={submitting}
                    color='grey'
                    content={actionLabel}
                />
            </Form>
        )}
    />
    )
}
SalaForm.propTypes={
    id:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
SalaForm.defaultProps={
    id:null
}
export default SalaForm
