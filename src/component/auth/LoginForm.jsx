import React from 'react'
import { Field, Form as FinalForm} from 'react-final-form'
import { combineValidators, isRequired } from 'revalidate'
import { FORM_ERROR } from 'final-form'
import { Form, Header, Button, Grid, Image } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import TextInput from '../form/TextInput'
import ErrorMessage from '../form/ErrorMessage'
import { login } from '../../app/store/actions/authActions'

const validate = combineValidators({
  username: isRequired('username'),
  password: isRequired('password'),
})
const actions = {
  login,
}
const LoginForm = ({login}) => {
    return (
       <FinalForm
      onSubmit={(values) => login(values).catch((error) => ({ [FORM_ERROR]: error }))}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
              <Image src='https://image.flaticon.com/icons/png/128/295/295128.png' /> Log-in to your account
            </Header>
            <Form onSubmit={handleSubmit} error>
              <Header as="h2"color="pink" textAlign="center" />
              <Field name="username" component={TextInput} placeholder="Type your username" />
              <Field name="password" component={TextInput} placeholder="Type your password" type="password" />
                {submitError && !dirtySinceLastSubmit && (
                  <ErrorMessage error={submitError} text="Invalid username or password" />
                )}
              <Button
                fluid
                disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                loading={submitting}
                color="violet"
                content="login"/>
                  {/* form come from object destructuring */}
                  {/* <pre>{JSON.stringify(form.getState(), null, 2)}</pre> */}
              </Form>
        </Grid.Column>
      </Grid>
      )}
      />
    )
}
LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
}
export default connect(null, actions)(LoginForm)
